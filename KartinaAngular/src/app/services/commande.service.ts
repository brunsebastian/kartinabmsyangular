import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Commande } from '../models/commande';
import { Article } from '../models/article';

@Injectable({
  providedIn: 'root'
})
export class CommandeService {

  private commandeUrl = 'http://localhost:8082/commande/commande'
  private articleUrl = 'http://localhost:8082/article/article'

  constructor(private _http: HttpClient) { }

  PostCommande(commande : Commande){
    return this._http.post(this.commandeUrl, commande);
  }
  PostArticle(article : Article){
    return this._http.post(this.articleUrl, article);
  }

}
