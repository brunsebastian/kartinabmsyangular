import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  private _themesUrl = 'http://localhost:8082/theme/theme';

  constructor(private _http: HttpClient) { }

  GetThemes(){
    return this._http.get(this._themesUrl);
  }
}
