import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  private userUrl = 'http://localhost:8082/user/user'

  constructor(private _http: HttpClient) { }

  PostUser(user : User){
    return this._http.post(this.userUrl, user);
  }
}
