import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ArtisteService {

  private _artisteUrl = 'http://localhost:8082/user/photographe'

  constructor(private _http: HttpClient) { }
  GetArtiste(){
    return this._http.get(this._artisteUrl);
  }
  GetArtisteId(id : number){
    return this._http.get(this._artisteUrl+ "/" + id);
  }
  GetPortfolio(id:number){
    return this._http.get(`http://localhost:8082/photo/portfolio/${id}`);
  }
}
