import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  private _photoUrl = 'http://localhost:8082/photo/photo'
  private _nouvUrl = 'http://localhost:8082/photo/nouveaute'
  private _topVentesUrl = 'http://localhost:8082/photo/top6Ventes'
  private _stockBasUrl = 'http://localhost:8082/photo/derniers'
  private _photoPaginerUrl = 'http://localhost:8082/photo/photos/paginate';

  constructor(private _http: HttpClient) { }

  GetPhoto(){
    return this._http.get(this._photoUrl);
  }
  GetNouveautes(){
    return this._http.get(this._nouvUrl);
  }
  GetTopVentes(){
    return this._http.get(this._topVentesUrl);
  }
  GetStockBas(){
    return this._http.get(this._stockBasUrl);
  }

  GetPhotoId(id : number){
    return this._http.get(this._photoUrl+ "/" + id);
  }

  GetPhotoPaginer(page: number, size: number) {
    return this._http.get(`${this._photoPaginerUrl}/${page}/${size}`);
  }
}
