import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FormatService {

  private _formatsUrl = 'http://localhost:8082/format/format';

  constructor(private _http: HttpClient) { }

  GetFormats(){
    return this._http.get(this._formatsUrl);
  }
}
