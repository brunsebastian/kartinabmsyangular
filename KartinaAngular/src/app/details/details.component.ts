import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { Photographie } from '../models/photographie';
import { ArtisteService } from '../services/artiste.service';
import { ActivatedRoute } from '@angular/router';
import { PhotoService } from '../services/photo.service';
import { Panier } from '../global/panier';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.sass']
})
export class DetailsComponent implements OnInit {
  photographe:User;
  selectedId:number;
  portfolio:Photographie[];
  photo:Photographie;

  constructor(private artisteService:ArtisteService,private photoService:PhotoService,private route:ActivatedRoute, private panier : Panier) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params=>{
      this.artisteService.GetArtisteId(+params.get('id')).subscribe((data)=>{
        this.photographe=data as User;
      });
      this.artisteService.GetPortfolio(+params.get('id')).subscribe((data) =>{
        this.portfolio = data as Photographie [];
        console.log(this.portfolio);
      });
    });
  }
  Ajouter(id : number){
    this.photoService.GetPhotoId(id).subscribe((data) =>{
      this.photo = data as Photographie;
      this.panier.listPhotoCommande.push(data as Photographie)
    });
  }
}
