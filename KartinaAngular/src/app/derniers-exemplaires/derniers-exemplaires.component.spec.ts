import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DerniersExemplairesComponent } from './derniers-exemplaires.component';

describe('DerniersExemplairesComponent', () => {
  let component: DerniersExemplairesComponent;
  let fixture: ComponentFixture<DerniersExemplairesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DerniersExemplairesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DerniersExemplairesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
