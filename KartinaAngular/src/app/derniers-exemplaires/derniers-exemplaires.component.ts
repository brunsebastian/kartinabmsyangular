import { Component, OnInit } from '@angular/core';
import { Photographie } from '../models/photographie';
import { PhotoService } from '../services/photo.service';
import { Panier } from '../global/panier';

@Component({
  selector: 'app-derniers-exemplaires',
  templateUrl: './derniers-exemplaires.component.html',
  styleUrls: ['./derniers-exemplaires.component.sass']
})
export class DerniersExemplairesComponent implements OnInit {
  listPhotographie: Photographie[] = [];
  photo : Photographie;
  constructor(private photoService : PhotoService, private panier : Panier) { }

  ngOnInit(): void {
    this.photoService.GetStockBas().subscribe((data) =>{
      this.listPhotographie = data as Photographie [];
  });
  }
  Ajouter(id : number){
    this.photoService.GetPhotoId(id).subscribe((data) =>{
      this.photo = data as Photographie;
      this.panier.listPhotoCommande.push(data as Photographie)
    });
  }

}
