import { Component, OnInit } from '@angular/core';
import { User } from '../models/user'
import { ArtisteService } from '../services/artiste.service'

@Component({
  selector: 'app-artistes',
  templateUrl: './artistes.component.html',
  styleUrls: ['./artistes.component.sass']
})
export class ArtistesComponent implements OnInit {
  listArtistes: User[] = [];

  constructor(private artisteService:ArtisteService) { }

  ngOnInit(): void {
    this.artisteService.GetArtiste().subscribe((data) =>{
      this.listArtistes = data as User [];
  })
  }

}
