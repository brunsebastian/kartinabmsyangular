import { Component, OnInit, Input } from '@angular/core';
import { User } from '../models/user';
import { Adr } from '../models/adr';
import { Info } from '../models/info';
import { UserServiceService } from '../services/user-service.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.sass']
})
export class UserFormComponent implements OnInit {
  @Input() currentUser : User = new User();
  @Input() currentAdr : Adr = new Adr();
  listAdr : Adr[] = [];
  constructor(private userService : UserServiceService) { }

  ngOnInit(): void {
  }

  enregistrer(): void {
    let u = new User();
    u.email=this.currentUser.email;
    u.nom=this.currentUser.nom;
    u.prenom=this.currentUser.prenom;
    u.dateInscr=Date.now()+"";
    u.pwd=this.currentUser.pwd;
    u.profil=this.currentUser.profil;
    u.tel=this.currentUser.tel;
    u.civ=this.currentUser.civ;
    this.affecterAdr();
    u.adr=this.listAdr;
    this.putUser(u);
  }

  affecterAdr(){
    let a = new Adr();
    a.num=this.currentAdr.num;
    a.rue=this.currentAdr.rue;
    a.cp=this.currentAdr.cp;
    a.ville=this.currentAdr.ville;
    a.pays=this.currentAdr.pays;
    this.listAdr.push(a);
  }

  putUser(u){
    this.userService.PostUser(u).subscribe(
      () => {
        console.log('Enregistrement terminé !');
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
  }
}
