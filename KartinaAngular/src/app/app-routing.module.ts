import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PhotographiesComponent } from './photographies/photographies.component';
import { NouveautesComponent } from './nouveautes/nouveautes.component';
import { ArtistesComponent } from './artistes/artistes.component';
import { DetailsComponent } from './details/details.component';
import { PanierComponent } from './panier/panier.component';
import { HomeComponent } from './home/home.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { LignePanierComponent } from './ligne-panier/ligne-panier.component';
import { UserFormComponent } from './user-form/user-form.component';



const routes: Routes = [
  {
    path: 'photographies',
    component: PhotographiesComponent
},
{
  path: 'nouveautes',
  component: NouveautesComponent
},
{
  path: 'artistes',
  component: ArtistesComponent
},
{
  path: 'details/:id',
  component: DetailsComponent
},
{
  path: 'panier',
  component: LignePanierComponent
},
{
  path : 'home',
  component : HomeComponent
},

{path : 'connexion',
component :UserFormComponent
},

{path : 'connexion/inscription',
component :InscriptionComponent
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
