import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RatingModule } from 'ng-starrating';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PhotographiesComponent } from './photographies/photographies.component';
import { NouveautesComponent } from './nouveautes/nouveautes.component';
import { RouterModule, Routes } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { PanierComponent } from './panier/panier.component';
import { HomeComponent } from './home/home.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { LignePanierComponent } from './ligne-panier/ligne-panier.component';
import { TopVentesComponent } from './top-ventes/top-ventes.component';
import { Panier } from './global/panier';
import { DerniersExemplairesComponent } from './derniers-exemplaires/derniers-exemplaires.component';
import { ArtistesComponent } from './artistes/artistes.component';
import { UserFormComponent } from './user-form/user-form.component';




@NgModule({
  declarations: [
    AppComponent,
    PhotographiesComponent,
    NouveautesComponent,
    DetailsComponent,
    PanierComponent,
    HomeComponent,
    ConnexionComponent,
    InscriptionComponent,
    LignePanierComponent,
    TopVentesComponent,
    DerniersExemplairesComponent,
    ArtistesComponent,
    UserFormComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RatingModule,
    ReactiveFormsModule
  ],
  providers: [Panier],
  bootstrap: [AppComponent]
})
export class AppModule { }
