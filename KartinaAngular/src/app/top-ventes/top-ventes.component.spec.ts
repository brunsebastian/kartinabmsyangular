import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopVentesComponent } from './top-ventes.component';

describe('TopVentesComponent', () => {
  let component: TopVentesComponent;
  let fixture: ComponentFixture<TopVentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopVentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopVentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
