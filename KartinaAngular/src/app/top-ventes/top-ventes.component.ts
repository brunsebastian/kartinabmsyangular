import { Component, OnInit } from '@angular/core';
import { Photographie } from '../models/photographie';
import { PhotoService } from '../services/photo.service';
import { Panier } from '../global/panier';

@Component({
  selector: 'app-top-ventes',
  templateUrl: './top-ventes.component.html',
  styleUrls: ['./top-ventes.component.sass']
})
export class TopVentesComponent implements OnInit {
  listPhotographie: Photographie[] = [];
  photo : Photographie;
  constructor(private photoService : PhotoService, private panier : Panier) { }

  ngOnInit(): void {
    this.photoService.GetTopVentes().subscribe((data) =>{
      this.listPhotographie = data as Photographie [];
  })
  }
  Ajouter(id : number){
    this.photoService.GetPhotoId(id).subscribe((data) =>{
      console.log(data);
      this.photo = data as Photographie;
      this.panier.listPhotoCommande.push(data as Photographie)
      console.log(this.panier.listPhotoCommande.length)
    });
  }

}
