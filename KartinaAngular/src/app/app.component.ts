import { Component } from '@angular/core';
import { Photographie } from './models/photographie';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'kartina';
  listPhotoCommande : Photographie[];
  constructor(){
  }
  onRatingListUserClicked(eventValue : Photographie[]){
    this.listPhotoCommande = eventValue;
  }
}