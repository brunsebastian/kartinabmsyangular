import { Component, OnInit, Input, Output } from '@angular/core';
import { Photographie } from '../models/photographie';
import { PhotoService } from '../services/photo.service';
import { FormatService } from '../services/format.service';
import { ThemeService } from '../services/theme.service';

import{ Format } from '../models/format';
import { Theme } from 'src/app/models/theme';
import { Panier } from '../global/panier';

@Component({
  selector: 'app-photographies',
  templateUrl: './photographies.component.html',
  styleUrls: ['./photographies.component.sass']
})
export class PhotographiesComponent implements OnInit {
  @Output() listPhotoCommande : Photographie[] = [];
  listPhotographie: Photographie[] = [];
  listPhotographiePaginer: Photographie[] = [];
  listFormats: Format[] = [];
  listThemes: Theme[] = [];
  listDesphotoCopy: Photographie[] = [];
  selected: any = {};
  selectedFilter = '';
  selectedTheme = '';
  photo : Photographie;
  themeSelectionne: Theme = {};
  orientationSelectionnes: string[] = [];
  formatSelectionnes : string[] = [];
   page = 0;
   size = 6;
  pages: any[];
  constructor(private photoService : PhotoService , private formatService : FormatService , private themeService : ThemeService, private panier : Panier) {
   }
 

  ngOnInit(): void {
    this.getAlPhotographie();
    this.getAllPhotographiePaginer();

    this.formatService.GetFormats().subscribe((data2) =>{
      this.listFormats = data2 as Format [];
    });

    this.themeService.GetThemes().subscribe((data3) =>{
      this.listThemes = data3 as Theme [];
    });
  }
  getAllPhotographiePaginer() {
    this.photoService.GetPhotoPaginer(this.page, this.size).subscribe((data: any) => {
        this.listPhotographiePaginer = data.content;
        this.pages = new Array(data['totalPages']);
        console.log(this.pages);
    });
  }
  getAlPhotographie() {
    this.photoService.GetPhoto().subscribe((data) =>{
      console.log(data);
      this.listPhotographie = data as Photographie [];
      this.listDesphotoCopy = data as Photographie [];
    });
  }

  filtrerParFormat(event) {
    this.listPhotographiePaginer = this.listDesphotoCopy;
    if (this.orientationSelectionnes.includes(event.target.value)) {
      let index = this.orientationSelectionnes.indexOf(event.target.value);
      this.orientationSelectionnes.splice(index, 1);
    } else {
      this.orientationSelectionnes.push(event.target.value);
    }
    if (this.orientationSelectionnes.length != 0) {
      this.listPhotographiePaginer = this.filterPhotographie(this.orientationSelectionnes);
    } else {
      this.getAllPhotographiePaginer();
    }
   
  }
  setPage(i, event: any) {
      event.preventDefault();
      this.page = i;
      this.getAllPhotographiePaginer();
  }
  filterPhotographie(orientationSelectionee: string[]): Photographie[] {
    let imageArray = [];
      for(let i =0; i< orientationSelectionee.length; i++) {
        for (let j = 0; j < this.listPhotographie.length; j++) {
          if (orientationSelectionee[i] === this.listPhotographie[j].orientation) {
                imageArray.push(this.listPhotographie[j]);
          }
        }
      }
        return imageArray;
  }


  Ajouter(id : number){
    this.photoService.GetPhotoId(id).subscribe((data) =>{
      console.log(data);
      this.photo = data as Photographie;
      this.panier.listPhotoCommande.push(data as Photographie)
      console.log(this.panier.listPhotoCommande.length)
    });
  }

//lesthemes

changedTheme(themeCourant, event) { 
  this.themeSelectionne = themeCourant;
  this.listPhotographie = this.listDesphotoCopy;
 this.listPhotographiePaginer = this.fiterPhotographByTheme(event.target.value)
}

fiterPhotographByTheme(theme): Photographie[] {
  let listPhotoFiltrerTheme = []
  for(let i =0; i< this.listPhotographie.length; i++) {
    for(let j =0; j < this.listPhotographie[i].themes.length; j++) {
      let themeCourant = this.listPhotographie[i].themes[j];
      if (themeCourant.theme === theme) {
          listPhotoFiltrerTheme.push(this.listPhotographie[i]);
      }
    }
  }
  return listPhotoFiltrerTheme;
}


// format

filter(FormatSelectionnes: string[]): Photographie[] {
  let imageArray = [];
    for(let i =0; i< FormatSelectionnes.length; i++) {
      for (let j = 0; j < this.listPhotographie.length; j++) {
        if (FormatSelectionnes[i] === this.listPhotographie[j].dispoFormat) {
              imageArray.push(this.listPhotographie[j]);
        }
      }
    }
      return imageArray;
}

filtrerFormat(event) {
  this.listPhotographiePaginer = this.listDesphotoCopy;
  if (this.formatSelectionnes.includes(event.target.name)) {
    let index = this.formatSelectionnes.indexOf(event.target.name);
    this.formatSelectionnes.splice(index, 1);
  } else {
    this.formatSelectionnes.push(event.target.name);
  }
  let combinaison = this.findCombinaison();
  console.log(combinaison);
  if (combinaison === '000') {
    this.getAllPhotographiePaginer();
  } else {
    this.listPhotographiePaginer = this.listPhotographiePaginer.filter((photo: Photographie) => {
      return photo.dispoFormat === combinaison
    });
  }
 
}
findCombinaison() {
  let x = '0';
  let y = '0';
  let z = '0';
  if (this.formatSelectionnes.includes('grand')) {
    x = '1';
  }
  if (this.formatSelectionnes.includes('géant')) {
    y = '1';
  }
  if (this.formatSelectionnes.includes('collector')) {
    z = '1';
  }
  return `${x}${y}${z}`;
  // return '' + x + '' + y
}


}