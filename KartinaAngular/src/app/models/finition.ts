export class Finition {
    id: number;
  libelle: string;
  descr: string;
  prct: number;
  version: number;
}
