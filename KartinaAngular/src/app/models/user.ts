import { Adr } from './adr';
import { Info } from './info';

export class User {
  id: number;
  civ: string;
  nom: string;
  prenom: string;
  email: string;
  pwd: string;
  tel: string;
  profil: string;
  adr: Adr[];
  info: Info;
  dateInscr: string;
  version: number;
}
