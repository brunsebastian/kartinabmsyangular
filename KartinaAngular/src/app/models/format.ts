export class Format {
    id: number;
    libelle: string;
    descr: string;
    hauteur: number;
    largeur: number;
    prct: number;
    version: number;
}
