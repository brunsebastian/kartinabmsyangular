export class Tag {
  id: number;
  tag: string;
  version: number;
}
