export class Info {
  id: number;
  bio: string;
  twitter: string;
  facebook: string;
  pinterest: string;
  version: number;
}
