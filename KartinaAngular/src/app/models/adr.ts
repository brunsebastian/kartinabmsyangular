export class Adr {
  id: number;
  num: number;
  rue: string;
  cp: string;
  ville: string;
  pays: string;
  version: number;
  active: boolean;
}
