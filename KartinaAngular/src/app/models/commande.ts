import { Article } from './article';
import { User } from './user';

export class Commande {
  id: number;
  dateCommande: string;
  prixTotal: number;
  etat: string;
  articles: Article[];
  version: number;
  user: User;
}
