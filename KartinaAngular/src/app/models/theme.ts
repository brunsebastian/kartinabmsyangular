export class Theme {
    id?: number;
    theme?: string;
    descr?: string;
    version?: number;
}
