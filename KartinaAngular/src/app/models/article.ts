import { Cadre } from './cadre';
import { Finition } from './finition';
import { Format } from './format';
import { Photographie } from './photographie';

export class Article {
  id: number;
  photo: Photographie;
  qte: number;
  prixUnit: number;
  format: Format;
  finition: Finition;
  cadre: Cadre;
  version: number;
}

