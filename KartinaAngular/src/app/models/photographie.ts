import { Theme } from './theme';
import { Tag } from './tag';
import { User } from './user';

export class Photographie {
    id: number;
    titre: string;
    descr: string;
    dispoFormat: string;
    url: string;
    user: User;
    dateAjout: string;
    prixInitial: number;
    prixAffiche: number;
    hauteur: number;
    largeur: number;
    stockInitial: number;
    stockRestant: number;
    orientation: string;
    tags: Tag[];
    themes: Theme[];
    version: number;
}


