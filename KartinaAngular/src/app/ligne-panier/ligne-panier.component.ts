import { Component, OnInit, Input, Output } from '@angular/core';
import { Photographie } from '../models/photographie';
import { Article } from '../models/article';
import { Commande } from '../models/commande';
import { Adr } from '../models/adr';
import { createOptional } from '@angular/compiler/src/core';
import { Cadre } from '../models/cadre';
import { Finition } from '../models/finition';
import { Format } from '../models/format';
import { CommandeService } from '../services/commande.service';
import { Panier } from '../global/panier';
import { User } from '../models/user';

@Component({
  selector: 'app-ligne-panier',
  templateUrl: './ligne-panier.component.html',
  styleUrls: ['./ligne-panier.component.sass']
})
export class LignePanierComponent implements OnInit {
  listPhotoCommande : Photographie[] = this.panier.listPhotoCommande;
  listArticle : Article[] = [];
  Commande : Commande = new Commande();
  prixTotal : number = 0;
  prixEnCours : number = this.calculPrixPanier();

  constructor(private commandeService : CommandeService, private panier : Panier) { }

  ngOnInit(): void {
    
  }

  calculPrixPanier(){
    let prix : number = 0;
    for(let i=0; i<this.panier.listPhotoCommande.length; i++){
      prix += this.panier.listPhotoCommande[i].prixAffiche
    }
    return prix;
  }

  Delete(id : number){
    this.panier.listPhotoCommande.splice(id, 1);
    this.prixEnCours=this.calculPrixPanier();

  }

  validerCommande(){
    this.createListArticle();
    this.putListArticle();
    this.createCommande();
    this.putCommande();
    this.Commande=new Commande();
    this.listArticle=[];
    this.panier.listPhotoCommande = [];
  }

  putCommande(){
    this.commandeService.PostCommande(this.Commande).subscribe(
      () => {
        console.log('Enregistrement terminé !');
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
  }

  createCommande(){
    this.Commande.articles=this.listArticle;
    this.Commande.dateCommande=Date.now()+"";
    this.Commande.etat="attente";
    this.Commande.version=0;
    this.Commande.prixTotal=this.calculPrixCommande();
    this.Commande.user=new User();
   
  }

  calculPrixCommande(){
    for(let i=0; i<this.listArticle.length; i++){
      this.prixTotal += (this.listArticle[i].prixUnit*this.listArticle[i].qte);
    }
    return this.prixTotal;
  }

  putListArticle(){
    for(let i=0; i<this.listArticle.length; i++){
      this.commandeService.PostArticle(this.listArticle[i]).subscribe(
        () => {
          console.log('Enregistrement terminé !');
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
    }
  }

  createListArticle(){
    for(let i=0; i<this.panier.listPhotoCommande.length; i++){
      let a = new Article();
      a.photo=this.panier.listPhotoCommande[i];
      a.qte=1;
      a.prixUnit=this.panier.listPhotoCommande[i].prixAffiche;
      a.cadre=new Cadre();
      a.finition = new Finition();
      a.format=new Format();
      a.version=0;
      this.listArticle.push(a);
    }
  }

}
